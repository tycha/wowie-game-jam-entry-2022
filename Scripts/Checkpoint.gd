extends Area2D

enum Action { Jump, TurnLeft, TurnRight }

export(Action) var action = Action.Jump
export(float) var jump_impulse = 200.0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func object_entered(object: Node):
	if object is AIFriendo:
		match action:
			Action.Jump:
				object.jump(jump_impulse)
			Action.TurnLeft:
				object.turn_left()
			Action.TurnRight:
				object.turn_right()
	pass
