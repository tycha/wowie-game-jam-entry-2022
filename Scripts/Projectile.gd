extends Area2D
class_name Projectile

export var steer_force = 50.0
export var speed = 500.0

var target = null

var _velocity = Vector2.ZERO
var _acceleration = Vector2.ZERO

func start(_transform):
	global_transform = _transform
	_velocity = transform.x * speed

func _physics_process(delta):
	_acceleration += seek()
	_velocity += _acceleration * delta
	_velocity = _velocity.clamped(speed)
	rotation = _velocity.angle()
	position += _velocity*delta

func seek():
	var steer: = Vector2.ZERO
	if target:
		var desired_velocity = (target.position - position).normalized() * speed
		steer = (desired_velocity - _velocity).normalized() * steer_force
	return steer
