extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(float) var force_scalar = 0.5
export(float) var target_velocity = 300
export(NodePath) var AI_path
export(int) var hp = 3

var AI: Node2D

func _ready():
	AI = get_node(AI_path)
	
	pass # Replace with function body.


func _physics_process(_delta):
	var difference = AI.position - self.position
	var v = (self.linear_velocity - (difference.normalized() * target_velocity))
	
	self.apply_central_impulse(-v * force_scalar * _delta)
	if abs(get_linear_velocity().x) > target_velocity:
		set_linear_velocity(
			Vector2(
				target_velocity * sign(get_linear_velocity().x),
				get_linear_velocity().y
				)
			)
	#linear_velocity = linear_velocity.clamped(target_velocity)
	pass

func _on_Enemy_body_entered(body):
	if body is Player:
		self.get_child(3).play()
	print(body)
