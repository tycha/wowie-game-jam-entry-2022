class_name AIFriendo

extends RigidBody2D

export(float) var movement_impulse_per_second = 1.0
export(float) var max_speed = 200.0

var direction: float = 1

func _ready():
	pass

func _physics_process(delta):
	self.apply_central_impulse(Vector2(direction, 0) * movement_impulse_per_second * delta)
	# clamp max_speed
	if abs(get_linear_velocity().x) > max_speed:
		set_linear_velocity(
			Vector2(
				max_speed * sign(get_linear_velocity().x),
				get_linear_velocity().y
				)
			)
	pass

func jump(impulse: float):
	self.get_child(2).play()
	self.apply_central_impulse(Vector2(0, -impulse))


func turn_left():
	direction = -1
	self.get_child(0).scale = Vector2(direction, 1) * .17
	
	
func turn_right():
	direction = 1
	self.get_child(0).scale = Vector2(direction, 1) * .17

signal Lose
signal Win

func _on_AI_Friendo_body_entered(body):
	print(body)
	if body.is_in_group("Enemies") or body.is_in_group("LoseZone"):
		emit_signal("Lose")
	if body.is_in_group("WinZone"):
		emit_signal("Win")
	pass # Replace with function body.

signal Escape

func _input(event):
	pass
#	if event.is_action_pressed("escape"):
#		emit_signal("Escape")
