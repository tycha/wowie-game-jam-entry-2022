extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var size = get_viewport_rect().size
	
	self.scale = Vector2(size.x / 1024, size.y / 512)
	#print(self.scale)
	pass
