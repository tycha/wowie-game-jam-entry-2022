extends Actor

class_name Player

func _ready():
	
	pass

func _physics_process(delta):
	_velocity = speed * get_direction()
	
	if _velocity.x < 0:
		self.get_child(0).scale = Vector2(-.18, .18)
		self.get_child(1).scale = Vector2(-1, 1)
	elif _velocity.x > 0:
		self.get_child(0).scale = Vector2(.18, .18)
		self.get_child(1).scale = Vector2(1, 1)
	
	_velocity = move_and_slide(_velocity)

func get_direction() -> Vector2:
	return Vector2(
		Input.get_action_strength("player_right") - Input.get_action_strength("player_left"),
		Input.get_action_strength("player_down") - Input.get_action_strength("player_up")
	)



