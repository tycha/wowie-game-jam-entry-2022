extends Node

var lvl_node: Node = null

func _ready():
	#$EscapeMenu.hide()
	$GameOver.hide()
	#$WinScreen.hide()
	$LevelSelector.show()
	for button in get_node("LevelSelector/Rows/Buttons").get_children():
		if !("scene" in button):
			button.connect("pressed", self, "_on_ReloadLevelLoader")
		else:
			button.connect("pressed", self, "_on_Button_pressed", [button.scene])
	
	for button in get_node("GameOver/Rows/Buttons").get_children():
		print(button)
		if !("scene" in button):
			print("bruh")
			button.connect("pressed", self, "_on_ReloadLevelLoader")
		else:
			button.connect("pressed", self, "_on_Button_pressed", [button.scene])
	
	for button in get_node("WinScreen/Rows/Buttons").get_children():
		if !("scene" in button):
			button.connect("pressed", self, "_on_ReloadLevelLoader")
		else:
			button.connect("pressed", self, "_on_Button_pressed", [button.scene])
	
	for button in get_node("EscapeMenu/Rows/Buttons").get_children():
		if !("scene" in button):
			button.connect("pressed", self, "_on_ReloadLevelLoader")
		else:
			button.connect("pressed", self, "_on_Button_pressed", [button.scene])

func _on_Button_pressed(lvl: PackedScene):
	$LevelSelector.hide()
	lvl_node = lvl.instance()
	add_child(lvl_node)
	print(lvl_node)
	lvl_node.get_node("AI friendo").connect("Win", self, "_on_Win_condition")
	lvl_node.get_node("AI friendo").connect("Lose", self, "_on_Lose_condition")
	lvl_node.get_node("AI friendo").connect("Escape", self, "_on_Escape")
	lvl_node.pause_mode = Node.PAUSE_MODE_STOP

func _on_Win_condition():
	print("YOU WON")
	print(lvl_node)
	lvl_node.queue_free()
	lvl_node = null
	
	$EscapeMenu.hide()
	$WinScreen.show()

func _on_Lose_condition():
	print("YOU LOST")
	print(lvl_node)
	lvl_node.queue_free()
	lvl_node = null

	$EscapeMenu.hide()
	$GameOver.show()

func _on_Escape():
	if get_tree().paused:
		print("Unescaped")
		$EscapeMenu.hide()
		get_tree().paused = false
	else:
		print("Escaped")
		$EscapeMenu.show()
		get_tree().paused = true

func _on_ReloadLevelLoader():
	print("Reload Level Loader")
	get_tree().reload_current_scene()
