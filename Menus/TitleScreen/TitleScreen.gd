extends Control

func _ready():
	for button in $Menu/CenterRow/Buttons.get_children():
		button.connect("pressed", self, "_on_Switch_Scene",[button.scene])

func _on_Switch_Scene(scene):
	get_tree().change_scene_to(scene)
